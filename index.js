const fs = require("fs");
const express = require("express");
const middleware = require("./middleware/middleware.js");
const { error } = require("console");
const app = express();
const port = 8000;

app.use(middleware);

app.get('/', (req, res)=>{
    res.send("Hi Welcome to custom middleware");
});

app.get('/about', (req, res)=>{
    res.send("About Page");
});

app.get('/contact', (req, res)=>{
    res.send("Contact Page");
});

app.get('/logs', (req, res)=>{
    fs.readFile("./middleware/log.txt", "utf-8", (error, logs_file)=>{
        if(error){
            res.sendStatus(404);
        }else{
            res.send(logs_file);
        }
    }); 
});

app.listen(port, () => {
    console.log(`listening on ${port}`);
});