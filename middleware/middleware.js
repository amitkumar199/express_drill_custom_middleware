const { error } = require("console");
const fs = require("fs");
const path =require('path');

function middleware(req, res, next){
    let message = `${req.method} ${req.url}`;
    message+='<br>';
    fs.appendFile(path.join(__dirname,'log.txt'), message, (error)=>{
        if(error){
            console.log(error);
        }else{
            console.log("Appended");
        }
    });
    next();
};

module.exports = middleware;